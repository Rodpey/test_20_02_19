namespace Fun_Appointement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Activities",
                c => new
                {
                    IDActivity = c.Int(nullable: false, identity: true),
                    Plage = c.Int(nullable: false),
                    Date = c.DateTime(nullable: false, storeType: "date"),
                    Validated = c.Boolean(nullable: false),
                    IDTypeActivity = c.Int(nullable: false),
                    Street = c.String(maxLength: 100),
                    PostalCode = c.Int(nullable: false),
                    City = c.String(maxLength: 50),

                })
                .PrimaryKey(t => t.IDActivity)

                .ForeignKey("dbo.TypeActivity", t => t.IDTypeActivity)
                .Index(t => t.IDTypeActivity);
                
            
            CreateTable(
                "dbo.Groups",
                c => new
                    {
                        IDGroup = c.Int(nullable: false, identity: true),
                        NameGroup = c.String(),
                    })
                .PrimaryKey(t => t.IDGroup);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        IDUser = c.Int(nullable: false, identity: true),
                        Pseudo = c.String(nullable: false, maxLength: 20),
                        Password = c.String(nullable: false, maxLength: 20),
                        Street = c.String(maxLength: 100),
                        PostalCode = c.Int(nullable: false),
                        City = c.String(maxLength: 50),
                        Validated = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.IDUser);
            
            CreateTable(
                "dbo.TypeActivity",
                c => new
                    {
                        IDTypeActivity = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        MinUser = c.Int(nullable: false),
                        MaxUser = c.Int(nullable: false),
                        IDUser = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IDTypeActivity)
                .ForeignKey("dbo.Users", t => t.IDUser, cascadeDelete: true)
                .Index(t => t.IDUser);
            
            CreateTable(
                "dbo.__MigrationHistory",
                c => new
                    {
                        MigrationId = c.String(nullable: false, maxLength: 150),
                        ContextKey = c.String(nullable: false, maxLength: 300),
                        Model = c.Binary(nullable: false),
                        ProductVersion = c.String(nullable: false, maxLength: 32),
                    })
                .PrimaryKey(t => new { t.MigrationId, t.ContextKey });
            
            CreateTable(
                "dbo.Users_Groups",
                c => new
                    {
                        IDGroup = c.Int(nullable: false),
                        IDUser = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.IDGroup, t.IDUser })
                .ForeignKey("dbo.Groups", t => t.IDGroup, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.IDUser, cascadeDelete: true)
                .Index(t => t.IDGroup)
                .Index(t => t.IDUser);
            
            CreateTable(
                "dbo.Activities_Users",
                c => new
                    {
                        IDActivity = c.Int(nullable: false),
                        IDUser = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.IDActivity, t.IDUser })
                .ForeignKey("dbo.Activities", t => t.IDActivity, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.IDUser, cascadeDelete: true)
                .Index(t => t.IDActivity)
                .Index(t => t.IDUser);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Activities_Users", "IDUser", "dbo.Users");
            DropForeignKey("dbo.Activities_Users", "IDActivity", "dbo.Activities");
            DropForeignKey("dbo.Activities", "IDTypeActivity", "dbo.TypeActivity");
            DropForeignKey("dbo.Users_Groups", "IDUser", "dbo.Users");
            DropForeignKey("dbo.Users_Groups", "IDGroup", "dbo.Groups");
            //DropForeignKey("dbo.Activities", "IDGroup", "dbo.Groups");
            DropIndex("dbo.Activities_Users", new[] { "IDUser" });
            DropIndex("dbo.Activities_Users", new[] { "IDActivity" });
            DropIndex("dbo.Users_Groups", new[] { "IDUser" });
            DropIndex("dbo.Users_Groups", new[] { "IDGroup" });
            //DropIndex("dbo.Activities", new[] { "IDGroup" });
            DropIndex("dbo.Activities", new[] { "IDTypeActivity" });
            DropTable("dbo.Activities_Users");
            DropTable("dbo.Users_Groups");
            DropTable("dbo.__MigrationHistory");
            DropTable("dbo.TypeActivity");
            DropTable("dbo.Users");
            DropTable("dbo.Groups");
            DropTable("dbo.Activities");
        }
    }
}
