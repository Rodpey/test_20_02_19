﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Fun_Appointement.Controllers
{
    public class HomeController : Controller
    {
        public static int? ConnectedUserID = null;
        public static string ConnectedPseudo = null;
        public ActionResult Index()
        {
            LogHelper.WriteToFile("--------------------------------------------------------","Index");
            FormsIdentity formsIdentity = HttpContext.User.Identity as FormsIdentity;
            
            if (ConnectedUserID == null && formsIdentity != null && formsIdentity.IsAuthenticated && FormsAuthentication.IsEnabled)
            {
                int ID = Convert.ToInt32(formsIdentity.Name);
                using (Fun_Appointement_DBModel context = new Fun_Appointement_DBModel())
                {
                    ConnectedUserID = ID; ;
                    ConnectedPseudo = context.Users.Where(u => u.IDUser == ID).FirstOrDefault().Pseudo;
                }
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}