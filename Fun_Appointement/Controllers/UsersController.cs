﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Fun_Appointement;
using Fun_Appointement.Models;

namespace Fun_Appointement.Controllers
{
    public class UsersController : Controller
    {
        private Fun_Appointement_DBModel db = new Fun_Appointement_DBModel();

        // GET: Users
        public async Task<ActionResult> Index()
        {
            return View(await db.Users.ToListAsync());
        }

        // GET: Users/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            var temp =  db.UserPrefs.Where(u => u.IDUser == id)
                .Include(a => a.TypeActivity)
                .OrderBy(o => o.IndexPref).ToList();
            ViewBag.UserPref =temp;

            return View(user);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IDUser,Pseudo,Password,Street,PostalCode,City,Validated")] User user)
        {
            if (ModelState.IsValid)
            {
                user.Validated = true;
                db.Users.Add(user);
                await db.SaveChangesAsync();
                return RedirectToAction("Index", "Home");
            }

            return View(user);
        }

        // GET: Users/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.ListeTypeActivities = db.TypeActivities.ToList();
            ViewBag.UserPref = db.UserPrefs.Where(u => u.IDUser == id).OrderBy(o => o.IndexPref).ToList();
            
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }
        // GET: Users/Choix1
        public ActionResult Choix1(string id1, string id2, string id3)
        {
            int[] listPref = new int[4] { 0, Convert.ToInt16(id1), Convert.ToInt16(id2), Convert.ToInt16(id3) };
            int iduser = Convert.ToInt16(HomeController.ConnectedUserID);
            var listListPref = db.UserPrefs.Where(a => a.IDUser == iduser).ToList();
            foreach (var elem in listListPref)
            {
                db.UserPrefs.Remove(elem);
            }
            db.SaveChanges();
            for (var i = 3; i > 0; i--)
            {
                var userPref = new UserPrefs();
                var idtypact = listPref[i];
                if (db.UserPrefs.Where(a => a.IDUser == iduser && a.IDTypeActivity == idtypact).Count() == 0)
                {
                    userPref.IDUser = Convert.ToInt16(HomeController.ConnectedUserID);
                    userPref.IDTypeActivity = listPref[i];
                    userPref.IndexPref = i;
                    db.UserPrefs.Add(userPref);
                }
                else
                {
                    db.UserPrefs.Remove(db.UserPrefs.Where(a => a.IDUser == iduser && a.IDTypeActivity == idtypact).FirstOrDefault());
                    db.SaveChanges();
                    userPref.IDUser = Convert.ToInt16(HomeController.ConnectedUserID);
                    userPref.IDTypeActivity = listPref[i];
                    userPref.IndexPref = i;
                    db.UserPrefs.Add(userPref);

                }
                db.SaveChanges();
            }
            return RedirectToAction("Details", "Users", new { id = HomeController.ConnectedUserID });
        }

        // POST: Users/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IDUser,Pseudo,Password,Street,PostalCode,City,Validated")] User user)
        {
            if (ModelState.IsValid)
            {
                user.Validated = true;
                db.Entry(user).State = EntityState.Modified;
                await db.SaveChangesAsync();
                if (HomeController.ConnectedPseudo == "admin")
                    return RedirectToAction("Index");
                else
                    return RedirectToAction("Index", "Home");
            }
            return View(user);
        }

        // GET: Users/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = await db.Users.FindAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            User user = await db.Users.FindAsync(id);
            db.Users.Remove(user);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
