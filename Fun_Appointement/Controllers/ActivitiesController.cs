﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Fun_Appointement;
using Newtonsoft.Json;

namespace Fun_Appointement.Controllers
{
    public class ActivitiesController : Controller
    {
        private Fun_Appointement_DBModel db = new Fun_Appointement_DBModel();

        // GET: Activities
        public async Task<ActionResult> Index()
        {
            var activities = db.Activities.Include(a => a.TypeActivity);
            return View(await activities.ToListAsync());
        }





        // GET: Activities/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Activity activity = await db.Activities.FindAsync(id);
            if (activity == null)
            {
                return HttpNotFound();
            }
            return View(activity);
        }

        // GET: Activities/Create
        public ActionResult Create()
        {
            ViewBag.IDTypeActivity = new SelectList(db.TypeActivities, "IDTypeActivity", "Name");
            return View();
        }

        // POST: Activities/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "IDActivity,Date,IDTypeActivity,Street,PostalCode,City,End")] Activity activity)
        {
            if (ModelState.IsValid)
            {
                db.Activities.Add(activity);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.IDTypeActivity = new SelectList(db.TypeActivities, "IDTypeActivity", "Name", activity.IDTypeActivity);
            return View(activity);
        }


        // GET: Activities/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Activity activity = await db.Activities.FindAsync(id);
            if (activity == null)
            {
                return HttpNotFound();
            }
            ViewBag.IDTypeActivity = new SelectList(db.TypeActivities, "IDTypeActivity", "Name", activity.IDTypeActivity);
            return View(activity);
        }

        // POST: Activities/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IDActivity,Date,IDTypeActivity,Street,PostalCode,City,End")] Activity activity)
        {
            if (ModelState.IsValid)
            {
                db.Entry(activity).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.IDTypeActivity = new SelectList(db.TypeActivities, "IDTypeActivity", "Name", activity.IDTypeActivity);
            return View(activity);
        }

        // GET: Activities/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Activity activity = await db.Activities.FindAsync(id);
            if (activity == null)
            {
                return HttpNotFound();
            }
            return View(activity);
        }

        // POST: Activities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Activity activity = await db.Activities.FindAsync(id);
            db.Activities.Remove(activity);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public JsonResult GetEvents()
        {
            using (Fun_Appointement_DBModel dc = new Fun_Appointement_DBModel())
            {
                var activities = db.Activities.Include(a => a.TypeActivity).ToList();
                List<ActivityForJson> result = new List<ActivityForJson>();

                foreach (var elem in activities)
                {
                    ActivityForJson temp = new ActivityForJson();
                    temp.ActivityName = elem.TypeActivity.Name;
                    temp.City = elem.City;
                    temp.Date = elem.Date;
                    temp.IDActivity = elem.IDActivity;
                    temp.End = elem.End;
                    temp.PostalCode = elem.PostalCode;
                    temp.Street = elem.Street;
                    //temp.Validated = elem.Validated;
                    result.Add(temp);
                }

                var str = JsonConvert.SerializeObject(result);
                LogHelper.WriteToFile(str, "list event");
                return Json(result, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        public ActionResult SaveEvent(ActivityForJson a)
        {

            using (Fun_Appointement_DBModel dc = new Fun_Appointement_DBModel())
            {
                
                var b = new Activity()
                {
                    IDTypeActivity = 1,
                    City = a.City,
                    Date = a.Date,
                    IDActivity = a.IDActivity,
                    End = a.End,
                    PostalCode = a.PostalCode,
                    Street = a.Street
                };
                //LogHelper.WriteToFile("rer", "save event");
                dc.Activities.Add(b);
                dc.SaveChanges();
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
        }

        [HttpPost]
        public ActionResult DeleteEvent(int IDActivity)
        {
            var status = false;
            using (Fun_Appointement_DBModel dc = new Fun_Appointement_DBModel())
            {
                var v = dc.Activities.Where(z => z.IDActivity == IDActivity).FirstOrDefault();
                if (v != null)
                {
                    dc.Activities.Remove(v);
                    dc.SaveChanges();
                    status = true;
                }
            }
            return new JsonResult { Data = new { status = status } };
        }
    }
    public class ActivityForJson
    {
        public int IDActivity { get; set; }
        public DateTime Date { get; set; }
        //public bool Validated { get; set; }
        public int IDTypeActivity { get; set; }
        public string Street { get; set; }
        public int PostalCode { get; set; }
        public string City { get; set; }
        public string ActivityName { get; set; }
        public DateTime? End { get; set; }

    }
}
