﻿using Fun_Appointement;
using Fun_Appointement.Controllers;
using Fun_Appointement.Models;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

namespace WebApplication1.Controllers
{
    public class AuthentificationController : Controller
    {
        // GET: Authentification
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(User user)
        {
            if (user.Pseudo != null || user.Password != null)
            {
                using (var context = new Fun_Appointement_DBModel())
                {
                    if (context.Users.Any(n => n.Pseudo == user.Pseudo && n.Password == user.Password))
                    {
                        var connectedUser = context.Users.FirstOrDefault(n => n.Pseudo == user.Pseudo && n.Password == user.Password);
                        FormsAuthentication.SetAuthCookie(connectedUser.IDUser.ToString(), true);
                        HomeController.ConnectedUserID = connectedUser.IDUser;
                        HomeController.ConnectedPseudo = connectedUser.Pseudo;
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ViewBag.Message = "Identifiant/MDP incorrects";
                        return View("Login");
                    }
                }
            }
            else
            {
                ViewBag.Message = "Veuillez remplir les champs";
                return View("Login");
            }
        }
        public ActionResult Disconnect(string url)
        {
            HomeController.ConnectedUserID = null;
            HomeController.ConnectedPseudo = null;
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

    }
}