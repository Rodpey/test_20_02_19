﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Fun_Appointement;
using Newtonsoft.Json;

namespace FunAppointement.Controllers
{
    public class CalendarController : Controller
    {
        // GET: Calendar
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetActivitiesByUserByDate(DateTime datedeb, DateTime datefin)
        {
            var IDUser = Fun_Appointement.Controllers.HomeController.ConnectedPseudo;
            using (var context = new Fun_Appointement_DBModel())
            {
                var listActivite = context.Activities.Include(a => a.TypeActivity).Include(a => a.Users)
                    .Where(a => a.Date <= datedeb && a.Date <= datefin)
                    .ToList();
                List<JsonActivityByUserByDate> result = new List<JsonActivityByUserByDate>();
                foreach (var elem in listActivite)
                {
                    var temp = new JsonActivityByUserByDate();
                    temp.IDActivity = elem.IDActivity;
                    temp.Plage = elem.Plage;
                    temp.Date = elem.Date;
                    temp.Validated = elem.Validated;
                    temp.Street = elem.Street;
                    temp.PostalCode = elem.IDActivity;
                    temp.City = elem.City;
                    temp.ActivityName = elem.TypeActivity.Name;
                    foreach (var user in elem.Users)
                    { temp.pseudos.Add(user.Pseudo); }
                    result.Add(temp);

                }
                   LogHelper.WriteToFile(JsonConvert.SerializeObject(result),nameof(this.ToString));
                return Json(result, JsonRequestBehavior.AllowGet);

            }
        }
    }
    public class JsonActivityByUserByDate
    {
        public int IDActivity { get; set; }
        public int Plage { get; set; }
        public DateTime Date { get; set; }
        public bool Validated { get; set; }
        public string Street { get; set; }
        public int PostalCode { get; set; }
        public string City { get; set; }
        public string ActivityName { get; set; }
        public List<string> pseudos { get; set; }
    }
}