namespace Fun_Appointement
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Data.Entity;

    [Table("Users")]
    public partial class User
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User()
        {
            Activities = new HashSet<Activity>();
            Groups = new HashSet<Group>();
        }

        [Key]
        public int IDUser { get; set; }

        [Required]
        [StringLength(20)]
        public string Pseudo { get; set; }

        [Required]
        [StringLength(20)]
        public string Password { get; set; }

        [StringLength(100)]
        public string Street { get; set; }

        public int PostalCode { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        public bool Validated { get; set; }


        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Activity> Activities { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Group> Groups { get; set; }
    }


    //namespace Fun_Appointement.Models
    //    {
    //        public partial class Fun_AppointementUsers : DbContext
    //        {
    //            public Fun_AppointementUsers()
    //                : base("name=Users")
    //            {
    //            }

    //            public virtual DbSet<User> Users { get; set; }

    //            protected override void OnModelCreating(DbModelBuilder modelBuilder)
    //            {
    //                modelBuilder.Entity<User>()
    //                    .Property(e => e.Pseudo)
    //                    .IsUnicode(false);

    //                modelBuilder.Entity<User>()
    //                    .Property(e => e.Password)
    //                    .IsFixedLength();
    //            }
    //        }
    //    }
}
