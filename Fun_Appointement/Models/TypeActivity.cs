namespace Fun_Appointement
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TypeActivity")]
    public partial class TypeActivity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TypeActivity()
        {
            Activities = new HashSet<Activity>();
        }

        [Key]
        public int IDTypeActivity { get; set; }

        public string Name { get; set; }

        public int MinUser { get; set; }

        public int MaxUser { get; set; }

        public int? IDUser { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Activity> Activities { get; set; }
    }
}
