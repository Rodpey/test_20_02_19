namespace Fun_Appointement
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Activities")]
    public partial class Activity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Activity()
        {
            Users = new HashSet<User>();
        }

        [Key]
        public int IDActivity { get; set; }

        public int Plage { get; set; }

        [Column(TypeName = "date")]
        public DateTime Date { get; set; }

        [Column(TypeName = "date")]
        public DateTime? End { get; set; }

        public bool Validated { get; set; }

        public int IDTypeActivity { get; set; }

        [StringLength(100)]
        public string Street { get; set; }

        public int PostalCode { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        public virtual TypeActivity TypeActivity { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User> Users { get; set; }

    
    }
}
