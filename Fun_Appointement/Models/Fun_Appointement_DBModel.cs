namespace Fun_Appointement
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using global::Fun_Appointement.Models;

    public partial class Fun_Appointement_DBModel : DbContext
    {
        public Fun_Appointement_DBModel()
            : base("name=Fun_Appointement_DBModel")
        {
        }

        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<Activity> Activities { get; set; }
        public virtual DbSet<Group> Groups { get; set; }
        public virtual DbSet<TypeActivity> TypeActivities { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserPrefs> UserPrefs { get; set; }
        //public virtual DbSet<ActivitiesUsers> Activities_Users { get; set; }
        //public virtual DbSet<UserGroup> Users_Groups { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Activity>()
                .HasMany(e => e.Users)
                .WithMany(e => e.Activities)
                .Map(m => m.ToTable("Activities_Users").MapLeftKey("IDActivity").MapRightKey("IDUser"));

            modelBuilder.Entity<Group>()
                .HasMany(e => e.Users)
                .WithMany(e => e.Groups)
                .Map(m => m.ToTable("Users_Groups").MapLeftKey("IDGroup").MapRightKey("IDUser"));

            modelBuilder.Entity<TypeActivity>()
                .HasMany(e => e.Activities)
                //.WithRequired(e => e.TypeActivity)
                //.WillCascadeOnDelete(false)
                ;
        }
    }
}
