﻿namespace Fun_Appointement.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserPref")]
    public partial class UserPrefs
    {
         [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public UserPrefs()
        {
        }

        [Key]
        [Column(Order = 0)]
        public int IDUser { get; set; }
        [Key]
        [Column(Order = 1)]
        public int IDTypeActivity { get; set; }

        public int IndexPref { get; set; }

        public virtual TypeActivity TypeActivity { get; set; }

    }
}
